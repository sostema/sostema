<h1 align="center">👋</h1>
<h3 align="center">Creative outcast with love to create something new and unique!</h3>

- ✨ Interested in various topics like **DS/ML, backend engineering and game development**

- 👨‍🏫 Would love to find a **mentor in DS/ML related fields**

- 🌱 I’m currently learning **Go and Rust** (mainly for little pet projects)

- 👯 I’m looking to collaborate on **creating boilerplate repositories for faster and better hackathon experience**

- 🤝 I’m looking for help with **writing academic papers on NLP-related topics**

- 💬 Ask me about **machine learning pipelines on PyTorch (Lightning)**

<h3 align="left">Languages I happen to know: </h3>

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white)
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white)

<h3 align="left">Frameworks and libraries: </h3>

<h4 align="left">Data Science related: </h4>

![PyTorch](https://img.shields.io/badge/PyTorch-%23EE4C2C.svg?style=for-the-badge&logo=PyTorch&logoColor=white) (Lightning)
![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white)
![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white)
![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=for-the-badge&logo=scikit-learn&logoColor=white)

<h4 align="left">Backend related: </h4>

![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi)
![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)
![Redis](https://img.shields.io/badge/redis-%23DD0031.svg?style=for-the-badge&logo=redis&logoColor=white)

<hr>

<h3 align="left">Some Github related information: </h3> 

![](https://komarev.com/ghpvc/?username=sostema&color=009B77&style=for-the-badge)

<img align="center" src="https://github-readme-stats.vercel.app/api?username=sostema&count_private=true&show_icons=true&theme=radical&locale=en" alt="sostema" />
